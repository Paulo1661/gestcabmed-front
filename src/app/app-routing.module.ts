import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './auth/login/login.component';
import { SignupComponent } from './auth/signup/signup.component';
import { CalendarComponent } from './calendar/calendar.component';
import { AuthGuard } from './guards/auth.guard';
import { MedicalRecordComponent } from './medical-record/medical-record.component';
import { FichePatientComponent } from './patients/fiche-patient/fiche-patient.component';
import { PatientsComponent } from './patients/patients.component';
import { UsersComponent } from './users/users.component';
import { WelcomeComponent } from './welcome/welcome.component';

const routes: Routes = [
  {path: '', component: WelcomeComponent},
  {path: 'login', component: LoginComponent},
  {path: 'signup', component: SignupComponent},
  {path: 'medical-record', component: MedicalRecordComponent, canActivate: [AuthGuard]},
  {path: 'patients', component: PatientsComponent, canActivate: [AuthGuard]},
  {path: 'patient/:id', component: FichePatientComponent, canActivate: [AuthGuard]},
  {path: 'users', component: UsersComponent, canActivate: [AuthGuard]},
  {path: 'rdv', component: CalendarComponent, canActivate: [AuthGuard]},

  {path: '**', redirectTo:''}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [AuthGuard]
})
export class AppRoutingModule { }
