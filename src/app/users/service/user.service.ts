import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from 'src/app/auth/models/user.model';
import { UiService } from 'src/app/shared/ui.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  readonly host='http://192.168.1.13:8081/utilisateurs'

  constructor(private http: HttpClient, private uiService: UiService) { }

  getAllUsers() {
    this.uiService.loadingStateChange.next(true);
    return this.http.get<User[]>(this.host);
  }
}
