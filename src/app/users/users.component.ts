import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Subscription } from 'rxjs';
import { User } from '../auth/models/user.model';
import { UiService } from '../shared/ui.service';
import { UserService } from './service/user.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  displayedColumns=['name', 'email', 'telephone', 'adresse', 'dateNaissance', 'registerDate'];
  dataSource = new MatTableDataSource<User>();

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  private userServiceSubs: Subscription;
  public isLoading: boolean;
  private loadingSubs: Subscription;

  constructor(private userService: UserService, private uiService: UiService) { }

  ngOnInit(): void {
    this.loadingSubs = this.uiService.loadingStateChange.subscribe(isLoading =>{
      this.isLoading = isLoading
    });
    this.userServiceSubs=this.userService.getAllUsers().subscribe({
      next: data => {
        this.dataSource.data = data;
        this.uiService.loadingStateChange.next(false);
      },
      error: error => {
        this.uiService.loadingStateChange.next(false);
        console.log(error);
      }
    });
  }

  doFilter(filterValue: String): void{
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  ngOnDestroy(): void {
    this.userServiceSubs.unsubscribe();
    this.loadingSubs.unsubscribe();
  }

}
