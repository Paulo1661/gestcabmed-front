import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material.module';
import {FlexLayoutModule} from '@angular/flex-layout';
import {HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";

import {ScheduleModule, AgendaService, DayService, WeekService, WorkWeekService, MonthService, DragAndDropService, ResizeService } from '@syncfusion/ej2-angular-schedule';

import { WelcomeComponent } from './welcome/welcome.component';
import { LoginComponent } from './auth/login/login.component';
import { SignupComponent } from './auth/signup/signup.component';
import { MedicalRecordComponent } from './medical-record/medical-record.component';
import { HeaderComponent } from './navigation/header/header.component';
import { SidenavListComponent } from './navigation/sidenav-list/sidenav-list.component';
import { PatientListComponent } from './patients/patient-list/patient-list.component';
import { CalendarComponent } from './calendar/calendar.component';
import { NouveauPatientComponent } from './patients/nouveau-patient/nouveau-patient.component';
import { PatientsComponent } from './patients/patients.component';
import { FichePatientComponent } from './patients/fiche-patient/fiche-patient.component';

import { AuthService } from './auth/services/auth.service';
import { SecurityInterceptorService } from './auth/services/security-interceptor.service';
import { DocsComponent } from './patients/fiche-patient/docs/docs.component';
import { NoteComponent } from './patients/fiche-patient/note/note.component';
import { RdvsPatientComponent } from './patients/fiche-patient/rdvs-patient/rdvs-patient.component';
import { BasiqueComponent } from './patients/fiche-patient/info/basique/basique.component';
import { GeneraleComponent } from './patients/fiche-patient/info/generale/generale.component';
import { UsersComponent } from './users/users.component';
import { CreateRdvDialogComponent } from './calendar/create-rdv-dialog/create-rdv-dialog.component';
import { RdvPrisComponent } from './calendar/rdv-pris/rdv-pris.component';

@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponent,
    LoginComponent,
    SignupComponent,
    MedicalRecordComponent,
    HeaderComponent,
    SidenavListComponent,
    PatientListComponent,
    CalendarComponent,
    NouveauPatientComponent,
    PatientsComponent,
    FichePatientComponent,
    DocsComponent,
    NoteComponent,
    RdvsPatientComponent,
    BasiqueComponent,
    GeneraleComponent,
    UsersComponent,
    CreateRdvDialogComponent,
    RdvPrisComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    FlexLayoutModule,
    FormsModule,
    HttpClientModule,
    ScheduleModule
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: SecurityInterceptorService, multi: true},
    AuthService, AgendaService, DayService, WeekService, WorkWeekService, DragAndDropService, ResizeService, MonthService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
