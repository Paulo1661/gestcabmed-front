import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Consultation } from '../models/consultation.model';

@Injectable({
  providedIn: 'root'
})
export class MedicalRecordService {

  readonly host = 'http://192.168.1.13/patients/'

  constructor(private http: HttpClient) { }

  getConsultaionsByPatientId(patientId: string) {
    return this.http.get<Consultation[]>(this.host+patientId);
  }
}
