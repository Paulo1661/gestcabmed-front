export interface Consultation {
    certificatMedicalId: string;
    ordonnanceId: string;
    examenComplementaireId: string;
    resumeConsultationId: string;
    lettreConfrereId: string;
    examenId: string;
    dateExamen: Date;
    dossierMedicalId: string;
}