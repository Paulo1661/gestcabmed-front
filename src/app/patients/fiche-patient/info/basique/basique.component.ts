import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { CreateRdvDialogComponent } from 'src/app/calendar/create-rdv-dialog/create-rdv-dialog.component';
import { Rdv } from 'src/app/calendar/model/rdv.model';
import { RdvService } from 'src/app/calendar/services/rdv.service';
import { UiService } from 'src/app/shared/ui.service';

@Component({
  selector: 'app-basique',
  templateUrl: './basique.component.html',
  styleUrls: ['./basique.component.css']
})
export class BasiqueComponent implements OnInit, OnDestroy {

  rdv: Rdv;
  private okButtonSubs = new Subscription();
  private createRdvSubs: Subscription = new Subscription();

  constructor(public dialog: MatDialog, private rdvService: RdvService, private uiService: UiService) {}

  ngOnInit(): void {
    this.rdv = {
      nom: 'paul',
      prenom: 'henry',
      date: new Date(),
      etat: 'Demand',
      rdvId: ''
    };
    this.okButtonSubs = this.rdvService.okButton.subscribe({
      next: data => {
        if (data) {
          this.rdv.date=new Date(data);
          this.createRdvSubs=this.rdvService.createRdv(this.rdv).subscribe({
            next: res => {
              console.log(new Date(res.date))
              this.uiService.showSnackBar("Nous vous confirmerons le rdv pour : "+new Date(res.date), null, {duration: 3000,verticalPosition:'top'});
            },
            error: error => {
              this.uiService.showSnackBar("une erreur c'est produite", null, {duration: 3000,verticalPosition:'top'});
            }
          });
        }
      }
    });
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(CreateRdvDialogComponent, {
      width: '300px',
      data: {nom: this.rdv.nom, prenom: this.rdv.prenom, date: this.rdv.date, heure: this.rdv.heures, minutes: this.rdv.minutes}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.rdv.date=new Date(result.date);
    });
  }

  ngOnDestroy(): void {
    this.createRdvSubs.unsubscribe();
    this.okButtonSubs.unsubscribe();
  }

}
