import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BasiqueComponent } from './basique.component';

describe('BasiqueComponent', () => {
  let component: BasiqueComponent;
  let fixture: ComponentFixture<BasiqueComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BasiqueComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BasiqueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
