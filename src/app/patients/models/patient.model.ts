export class Patient {
    email?: String;
    nom: String;
    prenom: String;
    telephone: String;
    adresse: String;
    prochainRdv: Date;
    lastRdv: Date;
    registerDate: Date;
    codeCNAM?: String;
    validite?: 'Valide' | 'Invalide';
    sexe?: 'Homme' | 'Femme' | null;
}