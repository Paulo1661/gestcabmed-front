import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UiService } from 'src/app/shared/ui.service';
import { Patient } from '../models/patient.model';

@Injectable({
  providedIn: 'root'
})
export class PatientService {
  patients: Patient[] = [
    {email: 'paulleufang@gmail.com', nom: 'Paul', prenom: 'Leufang', adresse: 'Route Tunis km 1', telephone: '+21656395925', prochainRdv: new Date(), lastRdv: new Date(), registerDate: new Date(), sexe: 'Homme', codeCNAM: '15hg256', validite: 'Valide'},
    {email: 'paulleufang@gmail.com', nom: 'Je sais pas', prenom: 'C\'est vrai', adresse: 'null part', telephone: 'sérieux', prochainRdv: new Date(), lastRdv: new Date(), registerDate: new Date(), sexe: null, codeCNAM: '15hg756', validite: 'Valide'}
  ];

  readonly host = 'http://192.168.1.13:8081/patients'

  constructor(private http: HttpClient, private uiservice: UiService) { }

  getPatients(): Patient[] {
    this.uiservice.loadingStateChange.next(true);
    return this.patients.slice();
  }

  getAllPatients() {
    return this.http.get<Patient[]>(this.host);
  }
}
