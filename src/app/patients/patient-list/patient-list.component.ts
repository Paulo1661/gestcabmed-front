import { Component, OnInit, AfterViewInit, ViewChild, OnDestroy } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Subscription } from 'rxjs';
import { UiService } from 'src/app/shared/ui.service';
import { Patient } from '../models/patient.model';
import { PatientService } from '../services/patient.service';

@Component({
  selector: 'app-patient-list',
  templateUrl: './patient-list.component.html',
  styleUrls: ['./patient-list.component.css']
})
export class PatientListComponent implements OnInit, AfterViewInit, OnDestroy {
  displayedColumns=['name', 'telephone', 'adresse', 'prochainRdv', 'lastRdv', 'registerDate'];
  dataSource = new MatTableDataSource<Patient>();

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  private patientsSubscription: Subscription;
  private loadingSubs: Subscription;
  public isLoading = true;

  constructor(private patientService: PatientService, private uiService: UiService) { }

  ngOnInit(): void {
    this.loadingSubs = this.uiService.loadingStateChange.subscribe(isLoading =>{
      this.isLoading = isLoading
    });
    this.patientsSubscription = this.patientService.getAllPatients().subscribe(
      {
        next: data => {
          this.dataSource.data = data;
          this.uiService.loadingStateChange.next(false);
        }
      }
    );
    //this.dataSource.data = this.patientService.getPatients();
  }


  doFilter(filterValue: String): void{
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  ngOnDestroy(): void {
    this.patientsSubscription.unsubscribe();
    this.loadingSubs.unsubscribe();
  }

}
