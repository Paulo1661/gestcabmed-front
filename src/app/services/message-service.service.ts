import { Injectable } from '@angular/core';
//import { SockJS } from 

// Declare SockJS and Stomp
declare var SockJS;
declare var Stomp;

@Injectable({
  providedIn: 'root'
})
export class MessageServiceService {

  public stompClient;
  public msg = ['un message', 'deux messages'];

  constructor() {
    //this.initializeWebSocketConnection();
   }

   initializeWebSocketConnection() {
    const serverUrl = 'http://192.168.1.13:8081/socket';
    const ws = new SockJS(serverUrl);
    this.stompClient = Stomp.over(ws);
    const that = this;
    // tslint:disable-next-line:only-arrow-functions
    this.stompClient.connect({}, function(frame) {
      that.stompClient.subscribe('/message', (message) => {
        if (message.body) {
          that.msg.push(message.body);
        }
      });
    });
  }

  sendMessage(message) {
    this.stompClient.send('/app/send/message' , {}, message);
  }
}
