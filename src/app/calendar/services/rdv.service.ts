import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { Rdv } from '../model/rdv.model';

@Injectable({
  providedIn: 'root'
})
export class RdvService {

  readonly host='http://192.168.1.13:8081/rdv';
  public okButton = new Subject<Date>();

  constructor(private http: HttpClient) { }

  createRdv(rdv: Rdv): Observable<Rdv> {
    if (rdv.rdvId == '') {
      return this.http.post<Rdv>(this.host, rdv);
    } else {
      console.log(rdv);
      return this.updateRdv(rdv);
    }
    
  }

  getRdvDemand() {
    return this.http.get<Rdv[]>(this.host+'/Demand');
  }

  getRdvConfirmed() {
    return this.http.get<Rdv[]>(this.host+'/Confirmed');
  }

  updateRdv(rdv: Rdv): Observable<Rdv>{
    return this.http.put<Rdv>(this.host, rdv);
  }
}
