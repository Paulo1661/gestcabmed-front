export interface Rdv {
    etat?: 'Demand' | 'Confirmed' | 'Passed';
    date: Date;
    heures?: number;
    minutes?: number;
    patientId?: string;
    nom: string;
    prenom: string;
    rdvId?: string;
}