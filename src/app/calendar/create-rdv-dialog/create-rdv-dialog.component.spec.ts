import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateRdvDialogComponent } from './create-rdv-dialog.component';

describe('CreateRdvDialogComponent', () => {
  let component: CreateRdvDialogComponent;
  let fixture: ComponentFixture<CreateRdvDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateRdvDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateRdvDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
