import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { UiService } from 'src/app/shared/ui.service';
import { RdvService } from '../services/rdv.service';

export interface DialogData {
  nom: string;
  prenom: string;
  date: Date;
  heures?: number;
  minutes?: number;
}

@Component({
  selector: 'app-create-rdv-dialog',
  templateUrl: './create-rdv-dialog.component.html',
  styleUrls: ['./create-rdv-dialog.component.css']
})
export class CreateRdvDialogComponent implements OnInit, OnDestroy {

  private createRdvSubs: Subscription = new Subscription();
  public rdvDate = new Date();

  constructor(
    public dialogRef: MatDialogRef<CreateRdvDialogComponent>,
     @Inject(MAT_DIALOG_DATA) public data: DialogData,
     private rdvService: RdvService,
     private uiService: UiService) {}

  ngOnInit(): void {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onOkClick(data: DialogData) {
    /*console.log('send');
    this.createRdvSubs=this.rdvService.createRdv({...data}).subscribe({
      next: res => {
        console.log('result');
        this.uiService.showSnackBar("Nous vous confirmerons le rdv pour : "+res.date.getDate, null, {duration: 3000,verticalPosition:'top'});
      },
      error: error => {
        this.uiService.showSnackBar("une erreur c'est produite", null, {duration: 3000,verticalPosition:'top'});
      }
    });*/
    this.rdvService.okButton.next(this.rdvDate);
  }

  ngOnDestroy(): void {
    this.createRdvSubs.unsubscribe();
  }

}
