import { Component, OnDestroy, OnInit } from '@angular/core';
import { EventSettingsModel, View } from '@syncfusion/ej2-angular-schedule';
import { Subscription } from 'rxjs';
import { RdvService } from './services/rdv.service';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.css']
})
export class CalendarComponent implements OnInit, OnDestroy {
  private dataSource = [];
  public eventData: EventSettingsModel ;

  public newViewMode: View = 'Month';

  private rdvSubs = new Subscription();

  constructor(private rdvService: RdvService) { }

  ngOnInit(): void {
    this.dataSource=[
      {
        Id: 'rdv1',
        Subject: 'Board Meeting',
        StartTime: new Date(),
        EndTime: new Date()
      },
      {
        Id: 'rdv2',
        Subject: 'Training session on JSP',
        StartTime: new Date(),
        EndTime: new Date()
      },
      {
        Id: 'rdv3',
        Subject: 'Sprint Planning with Team members',
        StartTime: new Date(),
        EndTime: new Date()
      }
    ];
    this.rdvSubs = this.rdvService.getRdvConfirmed().subscribe({
      next: data => {
        data.forEach(rdv => this.dataSource.push({
          Id: rdv.rdvId,
          Subject: rdv.nom + ' ' + rdv.prenom,
          StartTime: new Date(rdv.date),
          EndTime: new Date(rdv.date)
        }));
        this.eventData =  {
          dataSource: this.dataSource
        };
      }
    });
   
  }

  ngOnDestroy(): void {
    this.rdvSubs.unsubscribe();
  }

  

}
