import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { UiService } from 'src/app/shared/ui.service';
import { CreateRdvDialogComponent } from '../create-rdv-dialog/create-rdv-dialog.component';
import { Rdv } from '../model/rdv.model';
import { RdvService } from '../services/rdv.service';

@Component({
  selector: 'app-rdv-pris',
  templateUrl: './rdv-pris.component.html',
  styleUrls: ['./rdv-pris.component.css']
})
export class RdvPrisComponent implements OnInit, OnDestroy {

  displayedColumns=['nom', 'etat', 'date', 'actions'];
  dataSource = new MatTableDataSource<Rdv>();

  private rdvServiceSubs = new Subscription();
  private loadingSubs = new Subscription();
  private okButtonSubs: Subscription = new Subscription();
  private createRdvSubs: Subscription = new Subscription();
  private confirmedRdvSubs: Subscription = new Subscription();;

  private rdv: Rdv;

  isLoading=true;

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  

  constructor(private rdvService: RdvService, private uiService: UiService, public dialog: MatDialog, private router: Router) { }

  ngOnInit(): void {
    this.loadingSubs = this.uiService.loadingStateChange.subscribe(isLoading =>{
      this.isLoading = isLoading
    });
    this.rdvServiceSubs=this.rdvService.getRdvDemand().subscribe({
      next: data => {
        this.dataSource.data = data.map(r => {r.date = new Date(r.date); return r;});
        this.uiService.loadingStateChange.next(false);
      }
    });

    this.okButtonSubs = this.rdvService.okButton.subscribe({
      next: data => {
        if (data) {
          this.rdv.date=data
          this.createRdvSubs=this.rdvService.createRdv(this.rdv).subscribe({
            next: res => {
              console.log('result');
              console.log(res.date);
              this.uiService.showSnackBar("Nous vous confirmerons le rdv pour : "+res.date, null, {duration: 3000,verticalPosition:'top'});
            },
            error: error => {
              this.uiService.showSnackBar("une erreur c'est produite", null, {duration: 3000,verticalPosition:'top'});
            }
          });
        }
      }
    })
  }

  openDialog(rdv: Rdv): void {
    this.rdv=rdv;
    const dialogRef = this.dialog.open(CreateRdvDialogComponent, {
      width: '300px',
      data: rdv
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      //this.rdv = { ...result};
    });
  }

  doFilter(filterValue: String): void{
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  acceptRdv(rdv: Rdv) {
    rdv.etat='Confirmed';
    this.confirmedRdvSubs = this.rdvService.updateRdv(rdv).subscribe({
      next: () => {
        this.dataSource.data=this.dataSource.data.filter(x => x.etat=='Demand').map(r => {r.date = new Date(r.date); return r;});
        this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
          this.router.navigate(['/rdv']);
      }); 
      }
    })
  }

  ngOnDestroy(): void {
    this.rdvServiceSubs.unsubscribe();
    this.loadingSubs.unsubscribe();
    this.okButtonSubs.unsubscribe();
    this.createRdvSubs.unsubscribe();
    this.confirmedRdvSubs.unsubscribe();
  }
  
}
