import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RdvPrisComponent } from './rdv-pris.component';

describe('RdvPrisComponent', () => {
  let component: RdvPrisComponent;
  let fixture: ComponentFixture<RdvPrisComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RdvPrisComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RdvPrisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
