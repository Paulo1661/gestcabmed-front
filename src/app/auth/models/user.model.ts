export interface User {
    email: String;
    nom: String;
    prenom: String;
    adresse: String;
    telephone: String;
    dateNaissance: Date;
    registerDate: Date;
    utilisateurId: String;
}