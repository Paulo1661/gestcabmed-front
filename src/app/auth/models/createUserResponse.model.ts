export interface CreateUserResponse {
    email: String;
    nom: String;
    prenom: String;
    adresse: String;
    telephone: String;
    dateNaissance: Date;
    utilisateurId: String;
}