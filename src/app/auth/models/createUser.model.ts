export interface CreateUser {
    email: String;
    nom: String;
    prenom: String;
    adresse: String;
    telephone: String;
    dateNaissance: Date;
    password: String;
}