import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { Subject } from "rxjs/internal/Subject";
import { UiService } from "src/app/shared/ui.service";
import { AuthData } from "../models/auth-data.model";
import { CreateUser } from "../models/createUser.model";
import { CreateUserResponse } from "../models/createUserResponse.model";
import * as moment from "moment";
import { Observable } from "rxjs";
import { tap, shareReplay } from 'rxjs/operators';
//import { User } from "../models/user.model";

const HOST:string="http://localhost:8081";

@Injectable()
export class AuthService {
    authChange = new Subject<boolean>();
    private isAuthenticated: boolean = false;
    userAuth: CreateUserResponse;

    constructor(private router: Router, private uiService: UiService, private http: HttpClient) {}

    initAuthListener() {
        if (this.isLoggedIn()) {
            this.authChange.next(true);
            console.log('connect');
        }
        else {
            this.authChange.next(false);
            console.log('not connect');
        }
    }

    async registerUser(createUser: CreateUser): Promise<void> {
        this.uiService.loadingStateChange.next(true);
        try {
            await this.http.post<CreateUserResponse>(HOST+"/utilisateurs", createUser).subscribe({
                next: data => {
                    this.userAuth=data;
                    console.log(data);
                    this.uiService.loadingStateChange.next(false);
                    this.uiService.showSnackBar("Utilisateur Créé, veuillez vous connecter", null, {duration: 3000,verticalPosition:'top'});
                    this.router.navigateByUrl('login');
                },
                error: error => {
                    console.log(error);
                    this.uiService.loadingStateChange.next(false);
                }
            });
            //this.uiService.loadingStateChange.next(false);
        } catch (error) {
            this.uiService.loadingStateChange.next(false);
            this.uiService.showSnackBar(error.message, null, {duration: 3000,verticalPosition:'top'});
        }
    }

    async login(authData: AuthData): Promise<void> {
        this.uiService.loadingStateChange.next(true);
        try {
            await this.http.post<any>(HOST+"/utilisateurs/login", authData).subscribe({
                next: data => {
                    this.userAuth=data;
                    console.log(data);
                },
                error: error => {
                    console.log(error);
                }
            });
            this.uiService.loadingStateChange.next(false);
        } catch (error) {
            this.uiService.loadingStateChange.next(false);
            this.uiService.showSnackBar(error.message, null, {duration: 3000,verticalPosition:'top'});
        }
    }

    login2(authData: AuthData): Observable<string> {
        this.uiService.loadingStateChange.next(true);
        return this.http.post<string>(HOST+"/utilisateurs/login", authData, { responseType: 'text' as 'json' })
        .pipe( tap(res => this.setSession(res)), shareReplay() );
        
    }

    private setSession(authResult) {
        authResult = JSON.parse(authResult);
        const expiresAt = moment().add(authResult.expireAt,'milliseconds');
        localStorage.setItem('id_token', authResult.token);
        console.log(authResult.expireAt);
        localStorage.setItem("expires_at", JSON.stringify(expiresAt.valueOf()) );
    }  

    logout() {
        localStorage.removeItem("id_token");
        localStorage.removeItem("expires_at");
        this.authChange.next(false);
        this.router.navigateByUrl('/login');
    }

    public isLoggedIn() {
        return moment().isBefore(this.getExpiration());
    }

    isLoggedOut() {
        return !this.isLoggedIn();
    }

    getExpiration() {
        const expiration = localStorage.getItem("expires_at");
        const expiresAt = JSON.parse(expiration);
        return moment(expiresAt);
    }    
}