import { Component, OnInit, OnDestroy } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Subscription } from 'rxjs/internal/Subscription';
import { UiService } from 'src/app/shared/ui.service';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit,OnDestroy {

  isLoading: boolean = false;
  private loadingSubs: Subscription;
  maxDate: Date;

  constructor(private authService: AuthService, private uiService: UiService) { }

  ngOnInit(): void {
    this.maxDate = new Date();
    this.maxDate.setFullYear(this.maxDate.getFullYear()-18);
    this.loadingSubs = this.uiService.loadingStateChange.subscribe(isLoading =>{
      this.isLoading = isLoading
    });
  }

  async onSubmit(form: NgForm): Promise<void> {
    console.log(form);
    this.authService.registerUser({
      email: form.value.email,
      password: form.value.password,
      adresse: form.value.adresse,
      dateNaissance: form.value.birthDate,
      nom: form.value.lastName,
      prenom: form.value.firstName,
      telephone: form.value.tel
    });
  }

  ngOnDestroy(): void {
    this.loadingSubs.unsubscribe();
  }

}
