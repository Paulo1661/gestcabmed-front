import { Component, OnInit, OnDestroy } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/internal/Subscription';
import { UiService } from 'src/app/shared/ui.service';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit,OnDestroy {

  isLoading: boolean = false;
  private loadingSubs: Subscription;
  private loginSubscription: Subscription;

  constructor(private authService: AuthService, private uiService: UiService, private router: Router) { }

  ngOnInit(): void {
    this.loadingSubs = this.uiService.loadingStateChange.subscribe(isLoading =>{
      this.isLoading = isLoading
    });
    this.loginSubscription=new Subscription();
  }

  async onSubmit(form: NgForm): Promise<void> {
    console.log(form);
    this.authService.login({
      email: form.value.email,
      password: form.value.password
    });
  }

  login(form: NgForm) {
    this.loginSubscription=this.authService.login2({
      email: form.value.email,
      password: form.value.password
    }).subscribe({
      next: data => {
          console.log(data);
          this.uiService.loadingStateChange.next(false);
          this.authService.authChange.next(true);
          this.router.navigateByUrl('/patients');
      },
      error: error => {
          console.log(error);
          this.uiService.showSnackBar("Login ou mot de passe incorrect", null, {duration: 3000,verticalPosition:'bottom'});
          this.uiService.loadingStateChange.next(false);
      }
    });
  }

  ngOnDestroy(): void {
    this.loadingSubs.unsubscribe();
    this.loginSubscription.unsubscribe();
  }

}
