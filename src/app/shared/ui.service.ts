import { Injectable } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { Subject } from "rxjs/internal/Subject";

@Injectable({
  providedIn: 'root'
})
export class UiService {

  loadingStateChange = new Subject<boolean>();

  constructor( private snackbar: MatSnackBar) {}

  showSnackBar(message, action, config) {
      this.snackbar.open(message, action, config);
  }

}
